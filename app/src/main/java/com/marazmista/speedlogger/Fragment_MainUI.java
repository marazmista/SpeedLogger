package com.marazmista.speedlogger;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.app.Fragment;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.BIND_AUTO_CREATE;


public class Fragment_MainUI extends Fragment {
    BroadcastReceiver gnssDataReceiver;
    IntentFilter gnssFilter;
    ServiceConnection gnssConnection;
    boolean bounded;
    public GnssService gnssService;

    @BindView(R.id.t_speed) TextView t_speed;
    @BindView(R.id.t_maxSpeed) TextView t_maxSpeed;
    @BindView(R.id.t_averageSpeed) TextView t_averageSpeed;
    @BindView(R.id.t_gnssInfo) TextView t_satellites;
    @BindView(R.id.t_tripDistance) TextView t_tripDistance;
    @BindView(R.id.t_tripLengthTime) TextView t_tripLenghtTime;
    @BindView(R.id.t_tempo_km_min) TextView t_tempo_km_min;

    @BindView(R.id.pb_gettinFix) ProgressBar pb_gettinFix;

    @BindView(R.id.btn_pause) ImageButton btn_pause;
    @BindView(R.id.btn_start) ImageButton btn_start;

    static long secondsInMilli = 1000;
    static long minutesInMilli = secondsInMilli * 60;
    static long hoursInMilli = minutesInMilli * 60;


    public static String calcMilisecondsToString(long start, long currentTime, long pausedFor, Resources stringFormat) {
        long tripLengthInMillis = currentTime - start - pausedFor;
        return calcMilisecondsToString(tripLengthInMillis, stringFormat);
    }

    public static String calcMilisecondsToString(long tripLengthInMillis, Resources stringFormat) {
        long elapsedHours = tripLengthInMillis / hoursInMilli;
        tripLengthInMillis = tripLengthInMillis % hoursInMilli;

        long elapsedMinutes = tripLengthInMillis / minutesInMilli;
        tripLengthInMillis = tripLengthInMillis % minutesInMilli;

        return stringFormat.getString(R.string.lengthTimeFormat, elapsedHours, elapsedMinutes, tripLengthInMillis / secondsInMilli);
    }



    public Fragment_MainUI() {  }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main_ui, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    void setStartStopBtnLabel(boolean isRecording) {
        btn_start.setImageDrawable((isRecording) ? ContextCompat.getDrawable(getActivity(),R.mipmap.ic_stop) :  ContextCompat.getDrawable(getActivity(), R.mipmap.ic_start_record));
        btn_pause.setVisibility((isRecording) ? View.VISIBLE : View.INVISIBLE);

        if (!isRecording)
            btn_pause.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.mipmap.ic_pause));
    }

    static final int TRIPS = 0,
            SETTINGS = 1,
            ABOUT = 2,
            QUIT = 3;

    static final CharSequence menuItems[] = {"Trips", "Settings", "About", "Quit"};

    @OnClick(R.id.btn_menu)
    void showMenu() {

        new AlertDialog.Builder(getActivity(), R.style.theme_dialogIncFont)
                .setItems(menuItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case TRIPS:
                                Fragment_tripsList fragment_tripsList = new Fragment_tripsList();
                                getFragmentManager().beginTransaction().replace(R.id.mainContainer, fragment_tripsList, null).addToBackStack(null).commit();
                                break;

                            case SETTINGS:
                                Fragment_preferences fragment_preferences = new Fragment_preferences();
                                getFragmentManager().beginTransaction().replace(R.id.mainContainer, fragment_preferences, null).addToBackStack(null).commit();
                                break;

                            case ABOUT:
                                View dv = ((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_about, null);

                                TextView t_buildNumber = dv.findViewById(R.id.t_buildNumber);
                                TabHost host = dv.findViewById(R.id.tab_host);
                                host.setup();

                                TabHost.TabSpec spec = host.newTabSpec("tab_license");
                                spec.setContent(R.id.scroll_license);
                                spec.setIndicator(getString(R.string.tab_license));
                                host.addTab(spec);

                                spec = host.newTabSpec("tab_components");
                                spec.setContent(R.id.scroll_compoenents);
                                spec.setIndicator(getString(R.string.tab_components));
                                host.addTab(spec);
                                t_buildNumber.setText(getString(R.string.label_build, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE,
                                        DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT).format(new Date(BuildConfig.TIMESTAMP))));

                                new AlertDialog.Builder(getActivity(), R.style.theme_dialog)
                                        .setView(dv)
                                        .create().show();
                                break;

                            case QUIT:
                                quit();
                                break;
                        }
                    }
                }).show();
    }

    void quit() {
        if (gnssService.recordTrip) {
            startStopTripRecording();
            return;
        }

        Intent stopIntent = new Intent(getActivity(), GnssService.class);
        stopIntent.setAction(GnssService.ACTION_STOP_SERVICE);
        getActivity().startService(stopIntent);

        getActivity().finishAndRemoveTask();
    }

    @OnClick(R.id.btn_start)
    void startStopTripRecording() {
        if (gnssService.recordTrip) {
            View dv = ((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_endtrip, null);
            final AlertDialog d = new AlertDialog.Builder(getActivity(), R.style.theme_dialog)
                    .setView(dv)
                    .create();
            d.setCanceledOnTouchOutside(false);

            Button btn_save = dv.findViewById(R.id.btn_saveTrip);
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gnssService.endTrip();
                    setStartStopBtnLabel(gnssService.recordTrip);
                    d.dismiss();
                }
            });

            Button btn_dontSave = dv.findViewById(R.id.btn_dontSaveTrip);
            btn_dontSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gnssService.cancelTrip();
                    setStartStopBtnLabel(gnssService.recordTrip);
                    d.dismiss();
                }
            });

            Button btn_cancel = dv.findViewById(R.id.btn_cancel);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.dismiss();
                }
            });

            d.show();
            return;
        }

        gnssService.startTrip(Calendar.getInstance().getTimeInMillis());
        setStartStopBtnLabel(gnssService.recordTrip);
    }

    @OnClick(R.id.btn_pause)
    void pauseTripRecording() {
        btn_pause.setImageDrawable((gnssService.pauseTrip()) ? ContextCompat.getDrawable(getActivity(), R.mipmap.ic_record) : ContextCompat.getDrawable(getActivity(), R.mipmap.ic_pause));
    }


    @Override
    public void onResume() {
        super.onResume();

        gnssFilter = new IntentFilter();
        gnssFilter.addAction(SLactivity.RECEIVE_GNSS_DATA);

        gnssConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                bounded = true;
                GnssService.GnssServiceBinder binder = (GnssService.GnssServiceBinder)service;
                gnssService = binder.getGnssService();
                setStartStopBtnLabel(gnssService.recordTrip);

                if (!gnssService.gnssOn)
                    onGnssDisabled();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bounded = false;
            }
        };

        getActivity().bindService(((SLApplication)getActivity().getApplication()).serviceIntent, gnssConnection, BIND_AUTO_CREATE);

        gnssDataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (!intent.getAction().equals(SLactivity.RECEIVE_GNSS_DATA))
                    return;

                t_speed.setText(getResources().getString(R.string.default_value, intent.getExtras().getFloat(GnssService.SPEED, 0f)));
                t_averageSpeed.setText(getResources().getString(R.string.averageSpeeds, intent.getExtras().getFloat(GnssService.AVERAGE_SPEED_10KM, 0f),
                        intent.getExtras().getFloat(GnssService.AVERAGE_SPEED, 0f)));
                t_tempo_km_min.setText(getResources().getString(R.string.tempo_km_min_value, intent.getExtras().getFloat(GnssService.TEMPO_KM_MINUTES, 0f)));
                t_maxSpeed.setText(getResources().getString(R.string.default_value, intent.getExtras().getFloat(GnssService.MAX_SPEED,0)));
                t_satellites.setText(getResources().getString(R.string.gnssInfo, intent.getExtras().getFloat(GnssService.ACCURACY,0f),
                        intent.getExtras().getInt(GnssService.SAT_USED,0),
                        intent.getExtras().getInt(GnssService.SAT_COUNT,0)));
                t_tripDistance.setText(getResources().getString(R.string.default_value, intent.getExtras().getFloat(GnssService.DISTANCE) / 1000));

                if (gnssService.gnssOn)
                    pb_gettinFix.setVisibility((intent.getExtras().getBoolean(GnssService.HAS_FIX)) ? View.GONE : View.VISIBLE);
                else
                    onGnssDisabled();

                if (gnssService.tripStartTime != 0)
                    t_tripLenghtTime.setText(calcMilisecondsToString(gnssService.tripStartTime , Calendar.getInstance().getTimeInMillis(), 0, getResources()));
            }
        };

        getActivity().registerReceiver(gnssDataReceiver, gnssFilter);

        pb_gettinFix.setIndeterminate(true);

    }

    private void onGnssDisabled() {
        t_satellites.setText(getResources().getString(R.string.label_gnssOff));
        pb_gettinFix.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        if (gnssDataReceiver != null) {
            getActivity().unregisterReceiver(gnssDataReceiver);
            gnssDataReceiver = null;
        }

        getActivity().unbindService(gnssConnection);
        super.onPause();
    }
}
