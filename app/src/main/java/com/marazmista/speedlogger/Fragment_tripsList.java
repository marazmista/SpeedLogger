package com.marazmista.speedlogger;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.inloop.simplerecycleradapter.ItemClickListener;
import eu.inloop.simplerecycleradapter.ItemLongClickListener;
import eu.inloop.simplerecycleradapter.SettableViewHolder;
import eu.inloop.simplerecycleradapter.SimpleRecyclerAdapter;
import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

import com.marazmista.speedlogger.DbEntities.Trip;
import com.marazmista.speedlogger.DbEntities.TripEntry;
import com.marazmista.speedlogger.DbEntities.TripEntry_;
import com.marazmista.speedlogger.DbEntities.Trip_;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Fragment_tripsList extends Fragment implements ItemClickListener<Trip>, ItemLongClickListener<Trip> {
    @BindView(R.id.rv_trips) RecyclerView rv_trips;
    SimpleRecyclerAdapter<Trip> rv_adapter;
    Box<Trip> boxTrips;


    public class TripsViewholder extends SettableViewHolder<Trip> {
        @BindView(R.id.t_startDate) TextView t_startDate;
        @BindView(R.id.t_lenghtKm) TextView t_lengthKm;
        @BindView(R.id.t_lenghtTime) TextView t_lengthTime;
        @BindView(R.id.t_averageSpeed) TextView t_averageSpeed;
        @BindView(R.id.t_note) TextView t_note;

        @BindView(R.id.btn_graph) Button btn_graph;

        boolean expanded = false;

        public TripsViewholder(@NonNull Context context, @LayoutRes int layoutRes, @NonNull ViewGroup parent) {
            super(context, layoutRes, parent);
            ButterKnife.bind(this, itemView);

        }

        @Override
        public void setData(@NonNull Trip data) {
            t_startDate.setText(getResources().getString(R.string.label_tripDate, data.startTimeString));
            t_lengthKm.setText(getResources().getString(R.string.label_tripDistance, data.tripDistance / 1000));
            t_lengthTime.setText(getResources().getString(R.string.label_tripLengthTime, data.tripLengthTime));
            t_averageSpeed.setText(getResources().getString(R.string.label_tripAverageSpeed, data.averageSpeed));

            t_note.setText((data.note == null) ? getResources().getString(R.string.label_emptyNote) : data.note);

            // not expanded at start
            expanded = true;
            expand();
        }

        @Nullable
        @Override
        public List<? extends View> getInnerClickableAreas() {
            return Arrays.asList(btn_graph);
        }

        public void expand() {
            expanded = !expanded;
            int visibility = (expanded) ? View.VISIBLE : View.GONE;

            t_note.setVisibility(visibility);
        }
    }


    public Fragment_tripsList() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_trips_list, container, false);
        ButterKnife.bind(this, v);
        setRetainInstance(true);

        rv_trips.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_trips.setHasFixedSize(true);
        boxTrips = ((SLApplication)getActivity().getApplication()).boxStore.boxFor(Trip.class);

        rv_adapter = new SimpleRecyclerAdapter<>(this, new SimpleRecyclerAdapter.CreateViewHolder<Trip>() {
            @NonNull
            @Override
            protected SettableViewHolder<Trip> onCreateViewHolder(ViewGroup parent, int viewType) {
                return new TripsViewholder(parent.getContext(), R.layout.item_trip, parent);
            }

            @Override
            protected int getItemViewType(@NonNull Trip item, int position) {
                return 0;
            }
        }, boxTrips.getAll());

        rv_adapter.setLongClickListener(this);

        rv_trips.setAdapter(rv_adapter);
        return v;
    }

    public void updateTripNote(long tripId, String note) {
        Trip t = boxTrips.get(tripId);
        t.note = note;

        boxTrips.put(t);
        rv_adapter.notifyDataSetChanged();
        rv_trips.invalidate();
    }

    void deleteTrip(long tripId, int adapterPos) {
        boxTrips.remove(tripId);

        rv_adapter.removeItem(adapterPos);
        rv_adapter.notifyItemRemoved(adapterPos);
    }

    @Override
    public void onItemClick(@NonNull final Trip item, @NonNull final SettableViewHolder<Trip> viewHolder, @NonNull View view) {
        switch (view.getId()) {
            case R.id.btn_graph:
                Fragment_tripChart fragment_tripChart = new Fragment_tripChart();

                Bundle b = new Bundle();
                b.putLong("tripId", boxTrips.get(item.id).id);
                fragment_tripChart.setArguments(b);
                getFragmentManager().beginTransaction().replace(R.id.mainContainer, fragment_tripChart, null).addToBackStack(null).commit();
                return;
        }

        ((TripsViewholder)viewHolder).expand();
    }



    static final int EDIT_NOTE = 0,
            SHOW_STATS = 1,
            EXPORT_TO_TXT = 2,
            DELETE = 3;

    static final CharSequence menuItems[] = {"Edit note", "Show stats", "Export to txt", "Delete"};

    @Override
    public boolean onItemLongClick(@NonNull final Trip item, @NonNull final SettableViewHolder<Trip> viewHolder, @NonNull final View view) {
        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);


        new AlertDialog.Builder(view.getContext(), R.style.theme_dialogIncFont)
                .setItems(menuItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case EDIT_NOTE:
                        final long tripId = item.id;

                        View dv = getActivity().getLayoutInflater().inflate(R.layout.dialog_note, null);
                        final EditText edt_note = dv.findViewById(R.id.edt_note);
                        edt_note.setText(boxTrips.get(item.id).note);

                        AlertDialog d = new AlertDialog.Builder(view.getContext(), R.style.theme_dialog)
                                .setTitle(R.string.title_editNote)
                                .setView(dv)
                                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateTripNote(tripId, edt_note.getText().toString());
                                    }
                                }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).create();

//                d.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.appColorAccent));
//                d.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.appColorAccent));
                        d.setCanceledOnTouchOutside(false);
                        d.show();

                        return;
                    case SHOW_STATS:
                        showStats(item);

                        return;

                    case EXPORT_TO_TXT:
                        try {
                            File path = getActivity().getExternalFilesDir(null);
                            File file = new File(path, "Trip_" + boxTrips.get(item.id).startTimeString.replace(":", "-").replace(" ", "_") + ".txt");
                            StringBuilder sb = new StringBuilder();
                            sb.append("timestamp;longitude;latitude;speed;distance;accuracy;sats\n");
                            for (TripEntry te : boxTrips.get(item.id).tripEntries) {
                                sb.append(te.timestamp).append(";")
                                        .append(te.longitude).append(";")
                                        .append(te.latitide).append(";")
                                        .append(te.speed).append(";")
                                        .append(te.distanceSoFar).append(";")
                                        .append(te.accuracy).append(";")
                                        .append(te.sats_used).append("\n");
                            }
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(sb.toString().getBytes());
                            fos.close();
                            Toast.makeText(getActivity(), "Exported to " + path + file.getName(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {

                        }
                        return;

                    case DELETE:
                        new AlertDialog.Builder(getActivity(), R.style.theme_dialog)
                                .setTitle(R.string.msg_deleteTrip)
                                .setPositiveButton(R.string.btn_delete, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteTrip(item.id, viewHolder.getAdapterPosition());
                                    }
                                })
                                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                        return;
                }
            }
        }).show();

        return false;
    }

    private void showStats(@NonNull Trip item) {
        View dv = ((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_trip_stats, null);
        final AlertDialog d = new AlertDialog.Builder(getActivity(), R.style.theme_dialog)
                .setView(dv)
                .create();
        d.setCanceledOnTouchOutside(false);

        d.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                d.dismiss();
            }
        });

        Box<TripEntry> boxTripEntires = ((SLApplication)getActivity().getApplication()).boxStore.boxFor(TripEntry.class);

        // count total stops
        QueryBuilder<TripEntry> qTotalStops = boxTripEntires.query().between(TripEntry_.speed, 0, 0.01)
                .order(TripEntry_.id).and().greater(TripEntry_.distanceSoFar, 0);
        qTotalStops.backlink(Trip_.tripEntries).equal(Trip_.id, item.id);
        int totalStops = qTotalStops.build().property(TripEntry_.distanceSoFar).distinct().findFloats().length;


        // get all the tripEntires where speed is 0, to calc stops length
        QueryBuilder<TripEntry> qZeros = boxTripEntires.query().between(TripEntry_.speed, 0, 0.01)
                .order(TripEntry_.id).and().greater(TripEntry_.distanceSoFar, 0);
        qZeros.backlink(Trip_.tripEntries).equal(Trip_.id, item.id);

        List<TripEntry> zeros = qZeros.build().find();

        long totalStopTime = 0, idStopBegin = (zeros.size() > 0) ? zeros.get(0).id : 0;

        for (int i = 1; i < zeros.size(); ++i) {
            if (zeros.get(i).distanceSoFar != zeros.get(i - 1).distanceSoFar || i == zeros.size() - 1) {
                totalStopTime += zeros.get(i - 1).timestamp - item.tripEntries.getById(idStopBegin).timestamp;
                idStopBegin = zeros.get(i).id;
            }
        }


        float multipler = 10000;
        List<Pair<Long, Long>> listAccStats = new ArrayList<>();
        List<String> averages = new ArrayList<>();

        long idLower = 0, idUpper = 0;
        float higestDeltaV = 0;
        int higestDeltaVListId = 0, idStartAverage = 0;

        for (int i = 1; i < item.tripEntries.size(); ++i) {

            if (item.tripEntries.get(i).speed >= item.tripEntries.get(i - 1).speed) {
                if (idLower == 0)
                    idLower = item.tripEntries.get(i - 1).id;

                idUpper = item.tripEntries.get(i).id;

            } else {

                if (idLower != 0) {
                    float deltaV = item.tripEntries.getById(idUpper).speed - item.tripEntries.getById(idLower).speed;

                    if (deltaV > higestDeltaV) {
                        higestDeltaV = deltaV;
                        higestDeltaVListId = listAccStats.size();
                    }

                    listAccStats.add(new Pair<>(idLower, idUpper));
                }

                idLower = 0;
                idUpper = 0;
            }

            if (item.tripEntries.get(i).distanceSoFar - item.tripEntries.get(idStartAverage).distanceSoFar >= multipler || i == item.tripEntries.size() - 1) {
                averages.add(getString(R.string.label_averageItem, item.tripEntries.get(idStartAverage).distanceSoFar / 1000, item.tripEntries.get(i).distanceSoFar / 1000,
                        (multipler / (item.tripEntries.get(i).timestamp - item.tripEntries.get(idStartAverage).timestamp) * 1000) * 3.6));

                idStartAverage = i + 1;
            }
        }

        TextView txt_stops = dv.findViewById(R.id.txt_stops);
        txt_stops.setText(getString(R.string.label_stopsCount, totalStops));

        TextView txt_stopsLength = dv.findViewById(R.id.txt_stopsLength);
        txt_stopsLength.setText(getString(R.string.label_stopsTotalLength, Fragment_MainUI.calcMilisecondsToString(totalStopTime, getResources())));

        if (!listAccStats.isEmpty()) {
            TextView txt_accStats = dv.findViewById(R.id.txt_accStats);

            TripEntry lower = item.tripEntries.getById(listAccStats.get(higestDeltaVListId).first);
            TripEntry upper = item.tripEntries.getById(listAccStats.get(higestDeltaVListId).second);

            txt_accStats.setText(getString(R.string.label_accStat, lower.speed, upper.speed, upper.speed - lower.speed,  upper.distanceSoFar - lower.distanceSoFar,
                    Fragment_MainUI.calcMilisecondsToString(upper.timestamp - lower.timestamp, getResources())));
        }


        ListView listAverages = dv.findViewById(R.id.list_averages);
        ArrayAdapter<String> averagesAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_speed_average, averages);

        listAverages.setAdapter(averagesAdapter);

        d.show();
    }
}
