package com.marazmista.speedlogger;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import io.objectbox.BoxStore;
import com.marazmista.speedlogger.DbEntities.MyObjectBox;

public class SLApplication extends Application {
    public BoxStore boxStore;
    Intent serviceIntent;

    @Override
    public void onCreate() {
        super.onCreate();
        boxStore = MyObjectBox.builder().androidContext(SLApplication.this).build();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
