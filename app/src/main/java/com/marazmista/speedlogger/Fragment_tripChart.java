package com.marazmista.speedlogger;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.marazmista.speedlogger.DbEntities.Trip;
import com.marazmista.speedlogger.DbEntities.TripEntry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.objectbox.Box;


public class Fragment_tripChart extends Fragment {

    @BindView(R.id.chart_tripChart) LineChart tripSpeedChart;

    Box<Trip> boxTrips;
    Trip trip;

    class EntryData {
        long timestamp;
        float speed, distanceSoFar;

        EntryData(long ts, float s, float distance) {
            timestamp = ts;
            speed = s;
            distanceSoFar = distance;
        }
    }

    public Fragment_tripChart() {  }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_chart, container, false);
        ButterKnife.bind(this, v);

        boxTrips = ((SLApplication)getActivity().getApplication()).boxStore.boxFor(Trip.class);

        if (getArguments() == null)
            return v;

        trip = boxTrips.get(getArguments().getLong("tripId"));

        List<Entry> tripChartEntires = new ArrayList<>();

        for (TripEntry te : trip.tripEntries) {
            Entry e = new Entry(tripChartEntires.size(), te.speed);
            e.setData(new EntryData(te.timestamp, te.speed, te.distanceSoFar));
            tripChartEntires.add(e);
        }

        LineDataSet ldsSpeed = new LineDataSet(tripChartEntires, "Speed");
        ldsSpeed.setDrawCircles(false);
        ldsSpeed.setColor(Color.YELLOW);

        List<Entry> averageEntires = new ArrayList<>();
        Entry e = new Entry(0,0);
        averageEntires.add(new Entry(0, trip.averageSpeed));
        averageEntires.add(new Entry(tripChartEntires.size() - 1, trip.averageSpeed));

        LineDataSet ldsAverage = new LineDataSet(averageEntires, "Average");
        ldsAverage.setDrawCircles(false);
        ldsAverage.setColor(Color.WHITE);

        LineData ld = new LineData(ldsSpeed);
        ld.addDataSet(ldsSpeed);
        ld.addDataSet(ldsAverage);

        tripSpeedChart.getAxisRight().setEnabled(false);
        tripSpeedChart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        tripSpeedChart.getAxisLeft().setTextColor(Color.WHITE);
        tripSpeedChart.getAxisLeft().setTextSize(14);
        tripSpeedChart.getAxisLeft().setTextColor(Color.WHITE);
        tripSpeedChart.getLegend().setEnabled(false);
        tripSpeedChart.setDescription(new Description());

        tripSpeedChart.setData(ld);
        IMarker marker = new SpeedGraphMarkerView(getActivity(), R.layout.marker_chart);
        tripSpeedChart.setMarker(marker);

        tripSpeedChart.invalidate();

        return v;
    }

    public class SpeedGraphMarkerView extends MarkerView {

        private TextView t_time, t_speed, t_distance;

        public SpeedGraphMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            // find your layout components
            t_time = findViewById(R.id.t_time);
            t_speed =  findViewById(R.id.t_speed);
            t_distance = findViewById(R.id.t_distance);

            t_time.setTextColor(Color.WHITE);
            t_speed.setTextColor(Color.WHITE);
            t_distance.setTextColor(Color.WHITE);
        }

        @Override
        public void refreshContent(Entry e, Highlight highlight) {

            EntryData ed = (EntryData)e.getData();
            if (ed == null)
                return;

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            t_time.setText(sdf.format(ed.timestamp));
            t_speed.setText(getResources().getString(R.string.speedWithUnit, ed.speed));
            t_distance.setText(getResources().getString(R.string.distanceWithUnit, ed.distanceSoFar/ 1000));

            super.refreshContent(e, highlight);
        }

        private MPPointF mOffset;

        @Override
        public MPPointF getOffset() {

            if(mOffset == null) {
                // center the marker horizontally and vertically
                mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
            }
            return mOffset;
        }
    }
}
