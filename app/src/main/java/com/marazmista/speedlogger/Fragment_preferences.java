package com.marazmista.speedlogger;


import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;

public class Fragment_preferences extends PreferenceFragment {


    public Fragment_preferences() {
        // Required empty public constructor
    }


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }


}
