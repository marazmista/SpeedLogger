package com.marazmista.speedlogger.DbEntities;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class TripEntry {

    @Id
    public long id;

    public long timestamp;
    public double longitude, latitide;
    public float speed, accuracy, distanceSoFar;
    public int sats_used;

}
