package com.marazmista.speedlogger.DbEntities;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

@Entity
public class Trip {

    @Id
    public long id;

    public long startTime, endTime;
    public String startTimeString, note, tripLengthTime;
    public float tripDistance, averageSpeed;

    public ToMany<TripEntry> tripEntries;
}
