package com.marazmista.speedlogger;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import java.text.DateFormat;
import java.util.Calendar;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Pair;

import java.text.SimpleDateFormat;
import java.util.LinkedList;

import io.objectbox.Box;

import com.marazmista.speedlogger.DbEntities.Trip;
import com.marazmista.speedlogger.DbEntities.TripEntry;

public class GnssService extends Service {
    public static final String
            HAS_FIX = "fixok",
            ACCURACY = "accuracy",
            SPEED = "speed",
            MAX_SPEED = "maxspeed",
            AVERAGE_SPEED = "averageSpeed",
            AVERAGE_SPEED_10KM = "averageFor10km",
            TEMPO_KM_MINUTES = "km/min",
            SAT_USED = "sat_used",
            SAT_COUNT = "sat_count",
            LONGITUDE = "lon",
            LATITUDE = "lat",
            DISTANCE = "distance";

    final String CHANNEL_ID = "SpeedLoggerChannel";
    final int NOTIFICATION_ID = 1;

    final static String ACTION_STOP_SERVICE = "Stopservice";

    NotificationManager notificationManager;
    NotificationCompat.Builder notificationBuilder;
    LocationManager locationManager;
    LocationListener locationListener;
    Location prevLocation;
    GnssStatus.Callback gnssCallback;
    Intent dataIntent;
    IBinder binder;
    PowerManager.WakeLock wakeLock;

    long totalPauseTime = 0, currentPauseStart = 0, tripStartTime = 0;
    float tripDistance = 0, maxSpeed = 0;
    LinkedList<Pair<Long, Float>> lastKmTimestamps;
    Box<Trip> tripBox;
    Trip trip;
    boolean recordTrip = false, paused = false, gnssOn = false;

    public class GnssServiceBinder extends Binder {
        public GnssService getGnssService() {
            return GnssService.this;
        }
    }


    public GnssService() {
        dataIntent = new Intent();
        dataIntent.setAction(SLactivity.RECEIVE_GNSS_DATA);
        binder = new GnssServiceBinder();
        lastKmTimestamps = new LinkedList<>();
    }

    float calcAverageSpeed(float distance, long startTimestamp, long lastReadTimestamp) {
        return (float) ((distance / ((lastReadTimestamp - startTimestamp - totalPauseTime) / 1000)) * 3.6);
    }

    @Override
    public int onStartCommand (Intent intent,   int flags,  int startId) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            stopForeground(true);
            stopSelf();
            return START_STICKY;
        }

       if (intent.getAction() != null && intent.getAction().equals(ACTION_STOP_SERVICE)) {
            cleanUp();
            stopForeground(true);
            stopSelf();
            return START_STICKY;
        }

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "speedlogger:gnssServiceWakelock");
        wakeLock.acquire();

        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        gnssCallback = new GnssStatus.Callback() {
            @Override
            public void onSatelliteStatusChanged(GnssStatus status) {
                super.onSatelliteStatusChanged(status);
                int usedCount = 0;

                for (int i = 0; i < status.getSatelliteCount(); ++i) {
                    if (status.usedInFix(i))
                        usedCount++;
                }

                gnssOn = true;
                dataIntent.putExtra(SAT_COUNT, status.getSatelliteCount());
                dataIntent.putExtra(SAT_USED, usedCount);

                sendBroadcast(dataIntent);
            }
        };

        tripBox = ((SLApplication)getApplication()).boxStore.boxFor(Trip.class);

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }).start();

        // first record, for calculations without recording trip
        lastKmTimestamps.addFirst(new Pair<>(Calendar.getInstance().getTimeInMillis(), 0f));

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                long timestamp = Calendar.getInstance().getTimeInMillis();

                dataIntent.putExtra(LONGITUDE, location.getLongitude());
                dataIntent.putExtra(LATITUDE, location.getLatitude());
                dataIntent.putExtra(ACCURACY, location.getAccuracy());
                dataIntent.putExtra(HAS_FIX, true);

                float speed = (location.getSpeed() * 3600) / 1000;
                dataIntent.putExtra(SPEED, speed);
                maxSpeed = (maxSpeed < speed) ? speed : maxSpeed;
                dataIntent.putExtra(MAX_SPEED, maxSpeed);

                if (speed != 0 && prevLocation != null && !paused) {
//                    if (location.getAccuracy() / 3 < distance) {
                        tripDistance += location.distanceTo(prevLocation);
                        dataIntent.putExtra(DISTANCE, tripDistance);
//                    }
                }

                if (!recordTrip || !paused) {
                    dataIntent.putExtra(AVERAGE_SPEED, calcAverageSpeed(tripDistance, tripStartTime, timestamp));

                    if (tripDistance - lastKmTimestamps.getFirst().second >= 1000) {
                        dataIntent.putExtra(TEMPO_KM_MINUTES,  calcAverageSpeed(tripDistance - lastKmTimestamps.getFirst().second, lastKmTimestamps.getFirst().first, timestamp) / 60);

                        lastKmTimestamps.addFirst(Pair.create(timestamp, tripDistance));

                        if (lastKmTimestamps.size() <= 10 && lastKmTimestamps.size() > 0) {
                            Pair<Long, Float> furthestKm = lastKmTimestamps.getLast();

                            dataIntent.putExtra(AVERAGE_SPEED_10KM,  calcAverageSpeed(tripDistance - furthestKm.second, furthestKm.first, timestamp));

                            if (lastKmTimestamps.size() == 10)
                                lastKmTimestamps.removeLast();
                        }
                    }
                }

                sendBroadcast(dataIntent);

                if (recordTrip && !paused) {
                    TripEntry te = new TripEntry();
                    te.latitide = location.getLatitude();
                    te.longitude = location.getLongitude();
                    te.timestamp = timestamp;
                    te.distanceSoFar = tripDistance;

                    te.speed = speed;
                    te.accuracy = location.getAccuracy();
                    te.sats_used = dataIntent.getExtras().getInt(SAT_USED, 0);

                    trip.tripEntries.add(te);
                }

                prevLocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                gnssOn = true;
                sendBroadcast(dataIntent);
            }

            @Override
            public void onProviderDisabled(String provider) {
                dataIntent.putExtra(HAS_FIX, false);
                gnssOn = false;
                sendBroadcast(dataIntent);
            }
        };

        notificationManager = (NotificationManager)getSystemService(Service.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "SpeedLogger", NotificationManager.IMPORTANCE_LOW);
            channel.setSound(null, null);
            notificationManager.createNotificationChannel(channel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(getResources().getString(R.string.notif_on))
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(PendingIntent.getActivity(this, 0 , new Intent(this, SLactivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        Notification notification = notificationBuilder.build();
        notificationManager.notify(NOTIFICATION_ID, notification);

        locationManager.registerGnssStatusCallback(gnssCallback);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, locationListener);

        startForeground(NOTIFICATION_ID, notification);
        return START_STICKY;
    }

    public void startTrip(long startTime) {
        trip = new Trip();
        trip.startTime = startTime;
        tripStartTime = startTime;
        trip.startTimeString = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT).format(trip.startTime);

        // if start trip record, reset average
        tripDistance = 0;
        lastKmTimestamps.clear();
        lastKmTimestamps.addFirst(new Pair<>(startTime, 0f));

        maxSpeed = 0;

        recordTrip = true;
        paused = false;

        updateNotification(getResources().getString(R.string.notif_recording));
    }

    public boolean pauseTrip() {
        if (!recordTrip)
            return false;

        paused =  !paused;
        if (paused) {
            currentPauseStart = Calendar.getInstance().getTimeInMillis();
            updateNotification(getResources().getString(R.string.notif_paused));
        } else {
            totalPauseTime += Calendar.getInstance().getTimeInMillis() - currentPauseStart;
            updateNotification(getResources().getString(R.string.notif_recording));
        }

        return paused;
    }

    public void endTrip() {
        trip.endTime = Calendar.getInstance().getTimeInMillis();

        if (paused)
            totalPauseTime += trip.endTime - currentPauseStart;

        trip.tripDistance = tripDistance;

        trip.tripLengthTime = Fragment_MainUI.calcMilisecondsToString(trip.startTime, trip.endTime, totalPauseTime, getResources());
        trip.averageSpeed = calcAverageSpeed(tripDistance, tripStartTime, trip.endTime);

        tripBox.put(trip);

        cancelTrip();
    }

    public void cancelTrip() {
        recordTrip = false;
        paused = false;
        tripStartTime = 0;
        updateNotification(getResources().getString(R.string.notif_on));
    }

    public void updateNotification(String text) {
        notificationBuilder.setContentText(text);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    void cleanUp() {
        if (locationManager != null) {
            locationManager.unregisterGnssStatusCallback(gnssCallback);
            if (locationListener != null) {
                locationManager.removeUpdates(locationListener);
                locationListener = null;
            }
        }

        if (wakeLock != null && wakeLock.isHeld())
            wakeLock.release();
    }

    public void onDestroy () {
        cleanUp();
        ((SLApplication)getApplication()).serviceIntent = null;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
