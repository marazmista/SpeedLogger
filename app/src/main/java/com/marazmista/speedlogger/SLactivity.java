package com.marazmista.speedlogger;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;


public class SLactivity extends Activity {
    public static String RECEIVE_GNSS_DATA = "receiveGnssData";
    public static final int PERMISSION_REQUEST = 1;
    boolean savedInstanceExists = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        savedInstanceExists = savedInstanceState != null;

        if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST);
        } else
            createActivity();
    }

    void createActivity() {
        setContentView(R.layout.activity_slactivity);

        if (((SLApplication)getApplication()).serviceIntent == null) {
            ((SLApplication)getApplication()).serviceIntent = new Intent(this, GnssService.class);
            startService(((SLApplication)getApplication()).serviceIntent);
        }

        if (savedInstanceExists)
            return;

        Fragment_MainUI fragment_mainUI = new Fragment_MainUI();
        getFragmentManager().beginTransaction().add(R.id.mainContainer, fragment_mainUI, null).commit();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    finish();
                else
                    createActivity();
        }
    }
}
